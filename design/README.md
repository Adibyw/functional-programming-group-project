# Website Design

For this project, I tried to create a simple design that could be easily implemented by my friends. The pages only consist of a form that could be used by the user to input their data. After the data has been entered by the user, The page will add an element that contains the result of the computation from the backend based on the given data.

## Sneak Peak

This is the version I decided to use after many attempt of creating the website designs. Here is the landing page of the website.

![Landing Page](Landing_Page.jpg)

Now there is a few version for the page that will display the result of the computations. Which version to use will be based on time constraints and whether it is feasible to implement into our project. 

- Version 1:
![Detail versi 1](Detail_1.jpg)

- Version 2:
![Detail versi 2](Detail_2.jpg)

- Version 3:
![Detail versi 3](Detail_3.jpg)
