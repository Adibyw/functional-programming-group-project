# Functional Programming - Group Project

This repository contains our group project for Functional Programming Course in Computer Science Faculty at the University of Indonesia.
The theme for this group project is about implenting a finance engineering problem with the use of functional programming.

### Group Member:
* Adib Yusril Wafi
* Alif Ahsanil Satria
* Ezza Ardiala R.
* Izzan Fakhril Islam
* M. Ahnaf Zain Ilham

### How to run project

#### Backend

* Move to backend directory

  `cd frontend`

* Install all dependencies

  `cabal new-install` or `cabal install`

* Download the most recent list of packages

  `cabal new-update` or `cabal update`

* Start the server

  `cabal new-run` or `cabal run`
  
#### Frontend
* Install Node.js (https://nodejs.org/en/download/)
* Install typescript

    `npm install -g typescript`

* Move to frontend directory

    `cd frontend`

* Install the project dependecies

    `npm install`

* Start react app

    `npm start`
