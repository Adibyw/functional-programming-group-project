module MortgageSimulator where

payment :: Double -> Double -> Double -> Int
payment mortgageAmount numberOfYears yearlyInterestRate = round ((multipleMortgageAmountMonthlyInterestRate mortgageAmount yearlyInterestRate calculateInterestRate) / (calculatePenyebut yearlyInterestRate numberOfYears calculateInterestRate calculateYears))

multipleMortgageAmountMonthlyInterestRate :: Double -> Double -> (Double -> Double) -> Double -- contoh higher order functions
multipleMortgageAmountMonthlyInterestRate mortgage_amount yearly_interest_rate calculate_interest_rate = mortgage_amount * (calculate_interest_rate yearly_interest_rate)

calculatePenyebut :: Double -> Double -> (Double -> Double) -> (Double -> Int) -> Double
calculatePenyebut yearly_interest_rate number_of_years calculate_interest_rate calculate_years = 1 - (1/((1 + (calculate_interest_rate yearly_interest_rate))^ (calculate_years number_of_years)))
----------------------------------------------------------------------------------------------
calculateYears :: Double -> Int
calculateYears = round . multipleByYear -- total bulan hasil konversi dari tahun (returns the integer nearest argument between 0 and argument, inclusive)

multipleByYear :: Double -> Double
multipleByYear numberOfYears = numberOfYears * 12

----------------------------------------------------------------------------------------------
calculateInterestRate :: Double -> Double
calculateInterestRate =  dividePercentage . divideByYear -- persentase bunga per-bulan

divideByYear :: (Floating a) => a -> a
divideByYear = (/12)

dividePercentage :: (Floating a) => a -> a
dividePercentage = (/100)
