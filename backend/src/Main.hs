{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Main where
import Web.Scotty
import Network.HTTP.Types
import Data.Monoid ((<>))
import System.Environment
import Data.Aeson (FromJSON, ToJSON)
import GHC.Generics
import Response
import MortgageSimulator

data Mortgage = Mortgage { mortgage_amount :: Double, number_years :: Double, years_interest_rate :: Double } 
                    deriving (Show, Generic)
instance ToJSON Mortgage
instance FromJSON Mortgage

main =  do
    port <- read <$> getEnv "PORT"
    scotty port $ do
    -- contoh post request :
    -- {
    --     "mortgage_amount" : 510000000,
    --     "number_years" : 5,
    --     "years_interest_rate" : 10
    -- }

    -- contoh response : 
    -- {
    --     "yearsInterestRate": 10,
    --     "result": 1.0835992802746858e7,
    --     "mortgageAmount": 510000000,
    --     "numberYears": 5
    -- }
        post "/calculate-credits" $ do
            addHeader "Access-Control-Allow-Origin" "*"
            addHeader "Access-Control-Allow-Method" "POST"
            mortgage <- jsonData :: ActionM Mortgage -- Decode as Mortgage object
            let mortgageAmount = mortgage_amount mortgage
            let numberYears = number_years mortgage
            let yearsInterestRate = years_interest_rate mortgage
            let result = payment mortgageAmount numberYears yearsInterestRate

            json $ Response {
                mortgageAmount = mortgageAmount,
                numberYears = numberYears,
                yearsInterestRate = yearsInterestRate,
                result = result
            }
