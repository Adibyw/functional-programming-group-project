-- karena rumus menghitung kpr sangat bervariasi (tergantung dari bank yang mengadakan), maka
-- sebagai jalan tengah saya akan membuat rumusnya sesuai dengan yang ada pada website cermati
-- link : https://www.cermati.com/artikel/cara-menghitung-biaya-kpr-dan-cicilannya

-- contoh : 
-- asumsi harga rumah = 600 juta
-- uang muka = 15% x Harga Rumah = 15% x 600 = 90 juta

-- pokok kredit = harga rumah - uang muka
-- 600 - 90 = 510 juta

-- biaya provisi = 1% x pokok kredit = 5.1 juta

-- Pajak pembeli = 5% x (harga rumah - njoptkp)
-- NJOPTKP adalah Nilai Jual Objek Pajak Tidak Kena Pajak yang besarannya berbeda-beda setiap daerah
-- untuk wilayah jakarta, yakni sebesar 60 juta
-- Pajak pembeli = 5% x (600 juta - 60 juta) = 27 Juta

-- Penerimaan Negara Bukan Pajak (PNBP) = (1/1000 x harga rumah) + Rp50 ribu
-- PNBP = (1/1000 x 600 juta) + 50 ribu = 650 ribu

-- Biaya Balik Nama (BBN) = (1% x Harga Rumah) + Rp 500 ribu
-- BBN = (1% x 600 juta) + 500 ribu = 6.5 juta

-- Ada biaya selain dari yang sudah disebutkan di atas, yaitu biaya notaris (dalam hal ini kita
-- tidak dihitung karena sangat bervariasi antara 1 dengan yg lainnya)

-- Cicilan Per Bulan = (Pokok Kredit x Bunga Per Bulan) / [1-(1+ Bunga Per Bulan) ^(- Tenor dalam Satuan Bulan)] = (Rp510.000.000,- x 10%/12) / [1-(1+10%/12) ^(-60)] = Rp10.835.992,8

-- Total Pinjaman dan Bunga  = Cicilan Per Bulan x Tenor dalam Satuan Bulan = Rp10.835.992,80 x 60 = Rp650.159.568,16

-- Total Bunga = Total Pinjaman & Bunga - Pokok Kredit = Rp650.159.568,16 - Rp510.000.000,- = Rp140.159.568,16