{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Response where
import Web.Scotty
import Network.HTTP.Types
import Data.Monoid ((<>))
import Data.Aeson (FromJSON, ToJSON)
import GHC.Generics

data Response = Response { mortgageAmount :: Double, numberYears :: Double, yearsInterestRate :: Double, result :: Int } 
                    deriving (Show, Generic)

instance ToJSON Response
instance FromJSON Response