import { ADD_ARTICLE, GET_MORTGAGE } from "../constants";
import { AddArticle, ArticlePayload, GetMortgage } from "../types";

export function addArticle(payload: ArticlePayload): AddArticle {
    return {
        type: ADD_ARTICLE,
        payload
    };
}

export function getMortgage(payload: number): GetMortgage {
    return {
        type: GET_MORTGAGE,
        payload
    }
}

// Dividing actions to sub-category
export type Article = AddArticle;
export type Mortgage = GetMortgage;
