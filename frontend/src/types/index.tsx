// Place to define interface that will be used on the application

// Objects
export interface ArticlePayload {
    title: string;
    text: string;
    id: string;
}

// Action Types
export interface AddArticle {
    type: string;
    payload: ArticlePayload;
}

export interface GetMortgage {
    type: string;
    payload: number;
}
