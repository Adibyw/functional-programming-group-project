import { ADD_ARTICLE, GET_MORTGAGE } from "../constants";
import { ArticlePayload } from "../types";
import { Article, Mortgage } from "../actions";

interface articleData {
  articles: ArticlePayload[];
}

interface mortgageData {
  mortgage: number;
}

export function mortgageReducer(
  state: mortgageData = { mortgage: 0 },
  action: Mortgage
): mortgageData {
  switch (action.type) {
    case GET_MORTGAGE:
      return Object.assign({}, state, {
        mortgage: action.payload
      });
  }
  return state;
}

export function articleReducer(
  state: articleData = { articles: [] },
  action: Article
): articleData {
  switch (action.type) {
    case ADD_ARTICLE:
      return Object.assign({}, state, {
        articles: state.articles.concat(action.payload)
      });
  }
  return state;
}

export type articleState = articleData;
export type mortgageState = mortgageData;
