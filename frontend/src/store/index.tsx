import { createStore, applyMiddleware } from "redux";
import { mortgageReducer } from "../reducers";
import { composeWithDevTools } from "redux-devtools-extension";

const store = createStore(
  mortgageReducer,
  composeWithDevTools(
    applyMiddleware(),
));

export default store;
