import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { ReduxHomeForm, MortgageContainer } from "../components";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    // styles for form
    title: {
      fontFamily: "Roboto",
      fontSize: "72px",
      textAlign: "center",
      color: "#FC766A",
      marginTop: "20px",
      marginBottom: "20px"
    },
    container: {
      background: "#5B84B1",
      width: "80%",
      height: "260px",
      margin: "auto",
      marginTop: "80px",
      borderRadius: "20px",
      padding: "5px"
    },
  })
);
const Home: React.FC<object> = () => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <div className={classes.container}>
        <p className={classes.title}>KREDIT PEMILIKAN RUMAH</p>
        <ReduxHomeForm />
      </div>
      <MortgageContainer />
    </React.Fragment>
  );
};

export default Home;
