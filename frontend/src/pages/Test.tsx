import React from 'react';
import List from '../components/List';
import Form from '../components/Form';

const Test: React.FC<{name: string, group: string}> = ({name, group}) => {
    return (
        <div>
            <h1>Hello, {name}!, We are from {group}.</h1>
            <List />
            <Form />
        </div>
    );
}

export default Test;
