import React from "react";
import { ConnectedMortgageTable } from "../components";

const MortgageContainer: React.FC<object> = () => {
    return(
        <ConnectedMortgageTable/>
    );
}

export default MortgageContainer;
