import React, { useState } from "react";
import axios from "axios";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { InputField } from "./index";
import { Mortgage, getMortgage } from "../actions";
import { connect } from "react-redux";

interface IGetMortgage {
  mockGetMortgage: (payload: number) => Mortgage;
}

interface IState {
  [key: string]: string
}


const mapDispatchToProps = (dispatch: any) => {
  return {
    mockGetMortgage: (mortgage: number) => dispatch(getMortgage(mortgage))
  };
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    // styles for form
    container: {
      margin: "auto",
      width: "90%",
      display: "flex",
      flexWrap: "wrap"
    },
    // styles for form container
    card: {
      margin: "auto",
      width: "80%",
      borderRadius: "8px"
    },
    title: {
      marginLeft: "30px",
      marginBottom: "10px"
    },
    button: {
      flexGrow: 1,
      background: "#FC766A",
      width: "138px",
      height: "50px",
      borderRadius: "20px",
      border: "none",
      color: "white",
      fontWeight: "bold",
      marginTop: "10%",
      marginLeft: "20px"
    }
  })
);

const HomeForm: React.FC<IGetMortgage> = ({ mockGetMortgage }) => {
  const classes = useStyles();

  const [state, setState] = useState<IState>({
    "Mortage_Amount": "",
    "Number_of_Years": "",
    "Yearly_Interest_Rate": ""
  });

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    setState({
      ...state,
      [e.target.name]: e.target.value
    })
  };

  const handleSubmit = (e: any) => {
    e.preventDefault();

    let data = {
      "mortgage_amount": parseFloat(state.Mortage_Amount),
      "number_years": parseFloat(state.Number_of_Years),
      "years_interest_rate": parseFloat(state.Yearly_Interest_Rate)
    };

    axios
      .post("http://funcpro-backend.herokuapp.com/calculate-credits", JSON.stringify(data))
      .then(res => {
        mockGetMortgage(res.data.result)
      })
      .catch(e => {console.log(e)});
  };

  return (
    <div className={classes.card}>
      <form className={classes.container} noValidate autoComplete="off">
        {Object.keys(state).map((form:string) => {
         console.log([state[form]]);
          return (
            <InputField
          id={form}
          label={form.replace(/_/g, " ")}
          value={state[form]}
          onChange={handleChange}
        />
          )
        })}
        <div>
          <button className={classes.button} onClick={handleSubmit}>
            CALCULATE
          </button>
        </div>
      </form>
    </div>
  );
};

const ReduxHomeForm = connect(null, mapDispatchToProps)(HomeForm);
export default ReduxHomeForm;
