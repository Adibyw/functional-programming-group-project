import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import { mortgageState } from "../reducers";
import Paper from "@material-ui/core/Paper";

interface IMortgageProps {
  mortgage: number;
}

const mapStateToProps = (state: mortgageState) => {
  return { mortgage: state.mortgage };
};

const useStyles = makeStyles({
  root: {
    width: "70%",
    margin: "auto",
    marginTop: "30px",
    overflowX: "auto",
    background: "#ffffff",
    color: "black"
  },
  table: {
    minWidth: 650,
  },
  title: {
    marginLeft: "15px",
  },
  tableText: {
    color: "white",
  },
});

const MortgageTable: React.FC<IMortgageProps> = ({ mortgage }) => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Paper elevation= {10} className={classes.root}>
        <h4 className={classes.title}>Monthly Mortgage Amount: RP. {mortgage}</h4>
      </Paper>
    </React.Fragment>
  );
};

const ConnectedMortgageTable = connect(mapStateToProps)(MortgageTable);
export default ConnectedMortgageTable;
