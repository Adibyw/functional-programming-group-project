import React from "react";
import { connect } from "react-redux";
import { articleState } from "../reducers";
import { ArticlePayload } from "../types";

interface IListProps {
  articles: ArticlePayload[];
}

// giving articles state to List components
// all state is given by reducers
const mapStateToProps = (state: articleState) => {
  return { articles: state.articles };
};

const connectedList: React.FC<IListProps> = ({ articles }) => {
  return (
    <ul>
      {articles.map((element: ArticlePayload) => {
        return (
          <li key={element.id}>
            {element.title} {element.text}
          </li>
        );
      })}
    </ul>
  );
};

const List = connect(mapStateToProps)(connectedList);
export default List;
