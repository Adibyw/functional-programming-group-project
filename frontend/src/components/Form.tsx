import React, {useState} from "react";
import { connect } from "react-redux";
import { addArticle, Article } from "../actions";
import { ArticlePayload } from "../types";

interface IFormProps {
    addNewArticle: (payload: ArticlePayload) => Article;
}

// defining which action to dispatch
const mapDispatchToProps = (dispatch: any) => {
    return ({
        addNewArticle: (article: ArticlePayload) => dispatch(addArticle(article)),
    });
};

const ConnectedForm: React.FC<IFormProps> = ({ addNewArticle }) => {
    const [state, setState] = useState({
        title: "",
        text: "",
    });

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => setState({
        ...state,
        [e.target.name]: [e.target.value],
    });

    const handleSubmit = (e: React.FormEvent<EventTarget>) => {
        e.preventDefault();
        addNewArticle({ title: state.title, text: state.text, id: state.title});
        setState({
            title: "",
            text: "",
        });
    };

    return (
        <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="title">Title</label>
          <input
            type="text"
            id="title"
            name="title"
            value={state.title}
            onChange={handleChange}
          />
          <label htmlFor="title">Text</label>
          <input
            type="text"
            id="title"
            name="text"
            value={state.text}
            onChange={handleChange}
          />
        </div>
        <button type="submit">SAVE</button>
      </form>
    );
};

const Form = connect(null, mapDispatchToProps)(ConnectedForm);
export default Form;
