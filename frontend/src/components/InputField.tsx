import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import { withStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 200,
      background: "#FFFFFF",
      borderRadius: "20px",
      boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
      paddingLeft: "15px"
    },
  })
);

const CssTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "#FC766A",
    },
    "& label": {
      color: "#FC766A",
      marginLeft: "15%",
      fontWeight: "bold",
    },
    "& MuiInput-underline:after": {
      borderBottomColor: "FC766A",
    }
  }
})(TextField);

interface IInputField {
  id: string;
  label: string;
  value: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

const InputField: React.FC<IInputField> = ({ label, id, value, onChange }) => {
  const classes = useStyles();

  const isANumber = (input: string): boolean => {
    // check if input is not a number
    if (isNaN(Number(input))) {
      return false;
    }
    return true;
  };
  if (isANumber(value) === false) {
    return (
      <TextField
        error
        id={id}
        name={id}
        label="Error"
        value={value}
        onChange={onChange}
        defaultValue="Hello World"
        className={classes.textField}
        margin="normal"
        helperText="Please insert using integer"
      />
    );
  }

  return (
    <div>
      <CssTextField
        id={id}
        name={id}
        className={classes.textField}
        value={value}
        label={label}
        InputProps={{disableUnderline: true}}
        margin="normal"
        onChange={onChange}
      />
    </div>
  );
};

export default InputField;
